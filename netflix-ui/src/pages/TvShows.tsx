import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import Navbar from '../components/Navbar';
import CardSlider from '../components/CardSlider';
import { onAuthStateChanged } from 'firebase/auth';
import { firebaseAuth } from '../utils/firebase-config';
import { useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { RootState } from '../types';
import { fetchMovies, getGenres, FetchMoviesPayload } from '../store';
import SelectGenre from '../components/SelectGenre';
import Slider from '../components/Slider';
import NotAvailable from '../components/NotAvailable';

function TvShows() {
  const [isScrolled, setIsScrolled] = useState(false);
  const movies = useSelector((state: RootState) => state.netflix.movies);
  const genres = useSelector((state: RootState) => state.netflix.genres);
  const genresLoaded = useSelector((state: RootState) => state.netflix.genresLoaded);

  const navigate = useNavigate();
  const dispatch = useDispatch<ThunkDispatch<RootState, undefined, any>>(); // Use ThunkDispatch as the type

  useEffect(() => {
    dispatch(getGenres());
  }, [dispatch]);

  useEffect(() => {
    if (genresLoaded) {
      const payload: FetchMoviesPayload = { genres: genres, type: 'tv' };
      dispatch(fetchMovies(payload));
    }
  }, [dispatch, genres, genresLoaded]);

  const [user, setUser] = useState<string | undefined>(undefined);

  useEffect(() => {
    const authStateListener = onAuthStateChanged(firebaseAuth, (currentUser) => {
      if (currentUser) setUser(currentUser.uid);
      else navigate('/login');
    });

    return () => {
      authStateListener(); // Unsubscribe the listener on component unmount
    };
  }, [navigate]);

  useEffect(() => {
    const handleScroll = () => {
      setIsScrolled(window.pageYOffset === 0 ? false : true);
    };

    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll); // Cleanup the scroll event listener on component unmount
    };
  }, []);

  return (
    <Container>
      <div className="navbar">
        <Navbar isScrolled={isScrolled} />
      </div>
      <div className="data">
        <SelectGenre genres={genres} type="tv" />
        {movies.length ? <Slider movies={movies} /> : <NotAvailable />}
      </div>
    </Container>
  );
}

const Container = styled.div`
  .data {
    margin-top: 8rem;
    .not-available {
      text-align: center;
      color: white;
      margin-top: 4rem;
    }
  }
`;

export default TvShows;
