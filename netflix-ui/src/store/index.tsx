import { configureStore, createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import axios from 'axios';
import { API_KEY, TMDB_BASE_URL } from '../utils/constans';

export interface Movie {
  id: number;
  name: string;
  image: string;
  genres: string[];
}

export interface Genre {
  id: number;
  name: string;
}

interface NetflixState {
  movies: Movie[];
  genresLoaded: boolean;
  genres: Genre[];
}

const initialState: NetflixState = {
  movies: [],
  genresLoaded: false,
  genres: [],
};

export const getGenres = createAsyncThunk('netflix/genres', async () => {
  const { data } = await axios.get<{ genres: Genre[] }>('https://api.themoviedb.org/3/genre/movie/list?api_key=4215e7f9489db9c5b72ba875252d3421');
  return data.genres;
});

const createArrayFromRawData = (array: any[], moviesArray: Movie[], genres: Genre[]) => {
  array.forEach((movie: any) => {
    const movieGenres: string[] = [];
    movie.genre_ids.forEach((genre: number) => {
      const name = genres.find(({ id }) => id === genre);
      if (name) movieGenres.push(name.name);
    });
    if (movie.backdrop_path)
      moviesArray.push({
        id: movie.id,
        name: movie?.original_name ? movie.original_name : movie.original_title,
        image: movie.backdrop_path,
        genres: movieGenres.slice(0, 3),
      });
  });
};

const getRawData = async (api: string, genres: Genre[], paging = false) => {
  const moviesArray: Movie[] = [];
  for (let i = 1; moviesArray.length < 60 && i < 10; i++) {
    const { data } = await axios.get<{ results: any[] }>(`${api}${paging ? `&page=${i}` : ''}`);
    createArrayFromRawData(data.results, moviesArray, genres);
  }
  return moviesArray;
};

interface FetchDataByGenrePayload {
  genre: number;
  type: string;
}

export const fetchDataByGenre = createAsyncThunk('netflix/genre', async ({ genre, type }: FetchDataByGenrePayload, thunkAPI) => {
  const {
    netflix: { genres },
  } = thunkAPI.getState() as { netflix: NetflixState };
  return getRawData(`https://api.themoviedb.org/3/discover/${type}?api_key=4215e7f9489db9c5b72ba875252d3421&with_genres=${genre}`, genres);
});

export interface FetchMoviesPayload {
  genres: string[];
  type: string;
}

export const fetchMovies = createAsyncThunk('netflix/trending', async ({ type }: FetchMoviesPayload, thunkAPI) => {
  const {
    netflix: { genres },
  } = thunkAPI.getState() as { netflix: NetflixState };
  return getRawData(`${TMDB_BASE_URL}/trending/${type}/week?api_key=${API_KEY}`, genres, true);
});

export const getUsersLikedMovies = createAsyncThunk('netflix/getLiked', async (email: string) => {
  const { data } = await axios.get<{ movies: Movie[] }>(`http://localhost:5000/api/user/liked/${email}`);
  return data.movies;
});

interface RemoveMovieFromLikedPayload {
  movieId: number;
  email: string;
}

export const removeMovieFromLiked = createAsyncThunk('netflix/deleteLiked', async ({ movieId, email }: RemoveMovieFromLikedPayload) => {
  const { data } = await axios.put<{ movies: Movie[] }>('http://localhost:5000/api/user/remove', {
    email,
    movieId,
  });
  return data.movies;
});

const NetflixSlice = createSlice({
  name: 'Netflix',
  initialState,
  reducers: {
    setGenres: (state, action: PayloadAction<Genre[]>) => {
      state.genres = action.payload;
      state.genresLoaded = true;
    },
    setMovies: (state, action: PayloadAction<Movie[]>) => {
      state.movies = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getGenres.fulfilled, (state, action) => {
      state.genres = action.payload;
      state.genresLoaded = true;
    });
    builder.addCase(fetchMovies.fulfilled, (state, action) => {
      state.movies = action.payload;
    });
    builder.addCase(fetchDataByGenre.fulfilled, (state, action) => {
      state.movies = action.payload;
    });
    builder.addCase(getUsersLikedMovies.fulfilled, (state, action) => {
      state.movies = action.payload;
    });
    builder.addCase(removeMovieFromLiked.fulfilled, (state, action) => {
      state.movies = action.payload;
    });
  },
});

export const store = configureStore({
  reducer: {
    netflix: NetflixSlice.reducer,
  },
});

export const { setGenres, setMovies } = NetflixSlice.actions;
