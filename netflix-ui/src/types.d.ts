// types.d.ts

// Import tipe-tipe yang diperlukan dari Redux
import { NetflixState } from '../store/netflix/types';

// Definisikan RootState
export interface RootState {
  netflix: NetflixState;
}
