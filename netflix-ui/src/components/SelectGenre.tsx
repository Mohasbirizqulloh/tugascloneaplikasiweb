import React from 'react';
import { useDispatch } from 'react-redux';
import styled from 'styled-components';
import { ThunkDispatch } from 'redux-thunk';
import { Action } from 'redux';
import { fetchDataByGenre, Genre } from '../store';

interface SelectGenreProps {
  genres: Genre[];
  type: string;
}

interface FetchDataByGenrePayload {
  genre: number;
  type: string;
}

const SelectGenre: React.FC<SelectGenreProps> = ({ genres, type }) => {
  const dispatch: ThunkDispatch<{}, {}, Action> = useDispatch();

  const handleGenreChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const payload: FetchDataByGenrePayload = {
      genre: parseInt(e.target.value),
      type,
    };

    dispatch(fetchDataByGenre(payload));
  };

  return (
    <Select className="flex" onChange={handleGenreChange}>
      {genres.map((genre) => (
        <option value={genre.id} key={genre.id}>
          {genre.name}
        </option>
      ))}
    </Select>
  );
};

const Select = styled.select`
  margin-left: 5rem;
  cursor: pointer;
  font-size: 1.4rem;
  background-color: rgba(0, 0, 0, 0.4);
  color: white;
`;

export default SelectGenre;
