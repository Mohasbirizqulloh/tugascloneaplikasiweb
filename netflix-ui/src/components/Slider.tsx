import React from 'react';
import styled from 'styled-components';
import CardSlider from './CardSlider';

interface SliderProps {
  movies: any[]; // Sesuaikan dengan tipe data yang digunakan untuk movies
}

const Slider: React.FC<SliderProps> = ({ movies }) => {
  const getMoviesFromRange = (from: number, to: number) => {
    return movies.slice(from, to);
  };

  return (
    <Container>
      <CardSlider data={getMoviesFromRange(0, 10)} title="Trending Now" />
      <CardSlider data={getMoviesFromRange(10, 20)} title="New Releases" />
      <CardSlider data={getMoviesFromRange(20, 30)} title="Blockbuster Movies" />
      <CardSlider data={getMoviesFromRange(30, 40)} title="Popular on Netflix" />
      <CardSlider data={getMoviesFromRange(40, 50)} title="Action Movies" />
      <CardSlider data={getMoviesFromRange(50, 60)} title="Epics" />
    </Container>
  );
};

const Container = styled.div``;

export default Slider;
