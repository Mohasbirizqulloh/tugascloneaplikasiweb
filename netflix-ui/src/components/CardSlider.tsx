import React, { useRef, useState } from 'react';
import styled from 'styled-components';
import { AiOutlineLeft, AiOutlineRight } from 'react-icons/ai';
import Card from './Card';

interface CardSliderProps {
  data: any[]; // Sesuaikan dengan tipe data yang digunakan untuk movieData pada komponen Card
  title: string;
}

class CardSlider extends React.Component<CardSliderProps> {
  listRef = React.createRef<HTMLDivElement>();
  state = {
    sliderPosition: 0,
    showControls: false,
  };

  handleDirection = (direction: string) => {
    const listElement = this.listRef.current;
    if (listElement) {
      const { sliderPosition } = this.state;
      let distance = listElement.getBoundingClientRect().x - 70;
      if (direction === 'left' && sliderPosition > 0) {
        listElement.style.transform = `translateX(${230 + distance}px)`;
        this.setState({ sliderPosition: sliderPosition - 1 });
      }
      if (direction === 'right' && sliderPosition < 4) {
        listElement.style.transform = `translateX(${-230 + distance}px)`;
        this.setState({ sliderPosition: sliderPosition + 1 });
      }
    }
  };

  render() {
    const { data, title } = this.props;
    const { showControls } = this.state;

    return (
      <Container className="flex column" showControls={showControls} onMouseEnter={() => this.setState({ showControls: true })} onMouseLeave={() => this.setState({ showControls: false })}>
        <h1>{title}</h1>
        <div className="wrapper">
          <div className={`slider-action left ${!showControls ? 'none' : ''} flex j-center a-center`}>
            <AiOutlineLeft onClick={() => this.handleDirection('left')} />
          </div>
          <div className="slider flex" ref={this.listRef}>
            {data.map((movie, index) => {
              return <Card movieData={movie} index={index} key={movie.id} />;
            })}
          </div>
          <div className={`slider-action right ${!showControls ? 'none' : ''} flex j-center a-center`}>
            <AiOutlineRight onClick={() => this.handleDirection('right')} />
          </div>
        </div>
      </Container>
    );
  }
}

const Container = styled.div<{ showControls: boolean }>`
  gap: 1rem;
  position: relative;
  padding: 2rem 0;
  h1 {
    margin-left: 50px;
  }
  .wrapper {
    .slider {
      width: max-content;
      gap: 1rem;
      transform: translateX(0px);
      transition: 0.3s ease-in-out;
      margin-left: 50px;
    }
    .slider-action {
      position: absolute;
      z-index: 99;
      height: 100%;
      top: 0;
      bottom: 0;
      width: 50px;
      transition: 0.3s ease-in-out;
      svg {
        font-size: 2rem;
      }
    }
    .none {
      display: none;
    }
    .left {
      left: 0;
    }
    .right {
      right: 0;
    }
  }
`;

export default CardSlider;
