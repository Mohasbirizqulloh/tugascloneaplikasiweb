import React from 'react';

class NotAvailable extends React.Component {
  render() {
    return <h1 className="not-available">No Movies available for the selected genre. Please select a different genre.</h1>;
  }
}

export default NotAvailable;
