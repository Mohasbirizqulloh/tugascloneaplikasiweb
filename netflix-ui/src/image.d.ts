declare module '*.jpg' {
  const value: string;
  export default value;
}
declare module '*.png' {
  const value: string;
  export default value;
}

declare module '*.mp4' {
  const src: string;
  export default src;
}

declare module '*.webp' {
  const value: string;
  export default value;
}
