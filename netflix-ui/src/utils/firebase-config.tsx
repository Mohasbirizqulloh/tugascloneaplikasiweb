import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
const firebaseConfig = {
  apiKey: 'AIzaSyAA_6R2Aru-0aHQllvBtSDwXLmZoCT6o2U',
  authDomain: 'netflix-clone-13e0e.firebaseapp.com',
  projectId: 'netflix-clone-13e0e',
  storageBucket: 'netflix-clone-13e0e.appspot.com',
  messagingSenderId: '388040695558',
  appId: '1:388040695558:web:74bde40b06add0c5c88887',
  measurementId: 'G-NX1Z9FL7HS',
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const firebaseAuth = getAuth(app);
