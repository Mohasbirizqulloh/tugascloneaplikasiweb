const UserController = require('../controllers/UserController');

const router = require('express').Router();

router.get('/liked/:email', UserController.getLikedMovies);
router.post('/add', UserController.addToLikedMovies);
router.put('/remove', UserController.removeFromLikedMovies);

module.exports = router;
