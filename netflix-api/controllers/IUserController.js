class IUserController {
  async getLikedMovies(email) {
    // Mengembalikan daftar film yang disukai oleh pengguna dengan email tertentu
  }

  async addToLikedMovies(email, data) {
    // Menambahkan film ke daftar film yang disukai oleh pengguna dengan email tertentu
  }

  async removeFromLikedMovies(email, movieId) {
    // Menghapus film dari daftar film yang disukai oleh pengguna dengan email tertentu
  }
}

module.exports = IUserController;
