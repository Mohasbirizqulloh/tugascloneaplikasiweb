const express = require('express');
const cors = require('cors');
const userRoutes = require('./routes/UserRoutes');
const mongoose = require('mongoose');

class App {
  constructor() {
    this.app = express();
    this.configureApp();
    this.connectDatabase();
    this.configureRoutes();
  }

  static getInstance() {
    if (!App.instance) {
      App.instance = new App();
    }
    return App.instance;
  }

  configureApp() {
    this.app.use(cors());
    this.app.use(express.json());
  }

  connectDatabase() {
    mongoose
      .connect('mongodb://localhost:27017/netflix', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      })
      .then(() => {
        console.log('DB Connection Successful');
      })
      .catch((err) => {
        console.log(err.message);
      });
  }

  configureRoutes() {
    this.app.use('/api/user', userRoutes);
  }

  startServer(port) {
    this.app.listen(port, () => {
      console.log(`Server started on port ${port}`);
    });
  }
}

const app = App.getInstance();
app.startServer(5000);
