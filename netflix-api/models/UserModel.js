const mongoose = require('mongoose');

class UserModel {
  static init() {
    const userSchema = new mongoose.Schema({
      email: {
        type: String,
        required: true,
        unique: true,
        max: 50,
      },
      likedMovies: Array,
    });

    return mongoose.model('users', userSchema);
  }
}

module.exports = UserModel.init();
