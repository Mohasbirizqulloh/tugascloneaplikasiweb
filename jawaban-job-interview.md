# No 1
#### Use Case Table User Netflix 
Key | Use Case | Prioritas | Status |
 --- | --- | --- | --- |
1 | Pengguna dapat menonton konten yang tersedia di dalam platform. | Tinggi | Completed |
2 | Pengguna dapat menyimpan konten film yang menarik untuk ditonton nanti dan menambahkannya ke dalam daftar list tontonan. | Sedang | Unplanned |
3 | Pengguna dapat mencari konten berdasarkan judul, genre, atau bintang film | Sedang | Planned |
4 | Menyediakan rekomendasi konten yang berdasarkan riwayat penonton dan rating yang diberikan oleh pengguna. | Sedang | Unplanned |
5 | Pengguna dapat menonton konten dengan akses offline | Rendah | Unplanned | 
6 | Pengguna dapat menonton konten dengan bahasa dan subtitle yang diinginkan | Sedang | Completed | 
7 | Pengguna dapat Memberikan ulasan dan rating pada konten yang telah ditonton | Sedang | Completed |
8 | Pengguna dapat mengirimkan saran atau feedback kepada Netflix | Rendah | Unplanned | 
9 | Pengguna dapat menggunakan fitur Fast Forward / Rewind untuk melompati bagian-bagian yang tidak diinginkan | Rendah | Unplanned |
10 | Pengguna dapat daftar berlangganan untuk melihat semua konten yang tersedia | Sedang | Unplanned |


#### Use Case Table Manajemen Perusahaan Netflix 
Key | Use Case | Prioritas | Status |
 --- | --- | --- | --- |
 1 | Menampilkan daftar film dan serial TV | Sedang | Completed |
2 | Menampilkan informasi film atau serial TV | Sedang | Completed |
3 | Memutar film atau episode serial TV	| Tinggi |  Completed |
4 |Membuat dan mengelola daftar tontonan pengguna| sedang | Planned |
5 | Menyediakan rekomendasi berdasarkan preferensi pengguna | Rendah | Unplanned |
6 | Menyediakan fitur pemilihan bahasa dan teks | Tinggi | Unplanned |
7 | Mengatur pembayaran langganan pengguna | Tinggi | Unplanned |
8 | Menyediakan fitur profil pengguna dengan preferensi kustom | Sedang | Unplanned |
9 | Menyediakan fitur pembaruan otomatis konten baru |Tinggi | Unplanned |
10 | Menyediakan fitur pengunduhan konten offline | Sedang | Planned |
11 |Menyediakan dukungan pelanggan melalui pusat bantuan online | Rendah | Unplanned |

#### Use Case Table Direksi Perusahaan (dashboard, monitoring, analisis) Netflix 
Key | Use Case | Prioritas | Status |
 --- | --- | --- | --- |
  1 | Menampilkan dashboard eksekutif dengan gambaran umum tentang kinerja perusahaan | Tinggi | Planned |
  2 | Melacak dan menganalisis pertumbuhan pelanggan, termasuk churn rate dan retention rate | Tinggi | Unplanned |
  3 | Memantau performa konten, termasuk popularitas film dan serial TV tertentu, serta tingkat penonton | Tinggi | Unplanned |
  4 | Mengumpulkan dan menganalisis data demografis dan preferensi pengguna | sedang | Unplanned |
  5 | Melakukan analisis persaingan untuk memahami tren industri dan posisi pasar | sedang | Unplanned |
  6 | Memantau kepuasan pelanggan melalui survei dan umpan balik | sedang | Unplanned |
  7 | Mengidentifikasi dan memperbaiki masalah teknis dalam platform | Tinggi | Unplanned |
  8 | Melakukan analisis penggunaan bandwidth dan throughput untuk meningkatkan pengalaman pengguna | Sedang | Unplanned |
  9 | Memantau kepatuhan terhadap kebijakan privasi dan perlindungan data | Tinggi | Unplanned |
  10 | Menyediakan laporan keuangan dan analisis keuangan kepada direksi | Tinggi | Unplanned |
  

# No 2
#### Class Diagram Netflix
![Jawaban-nomer-dua](https://gitlab.com/Mohasbirizqulloh/praktikum-pbo/-/raw/master/Screen%20Record%20UTS/Untitled_Diagram.drawio__18_.png)

# No 3
#### SOLID Design Principle
- Single Responsibility Principle (SRP): Setiap fungsi di dalam kode memiliki tanggung jawab tunggal yang jelas. Misalnya, fungsi getLikedMovies bertanggung jawab untuk mengambil daftar film yang disukai oleh pengguna, fungsi addToLikedMovies bertanggung jawab untuk menambahkan film ke daftar film yang disukai, dan fungsi removeFromLikedMovies bertanggung jawab untuk menghapus film dari daftar film yang disukai. Hal ini mencerminkan penerapan prinsip SRP. Selanjutnya Komponen-komponen dalam kode seperti createArrayFromRawData, getRawData, dan fungsi-fungsi async lainnya masing-masing bertanggung jawab untuk tugas-tugas spesifik. Misalnya, createArrayFromRawData bertanggung jawab untuk membuat array film dari data mentah, dan getRawData bertanggung jawab untuk mendapatkan data mentah dari API. Ini mencerminkan prinsip SRP dengan memisahkan tugas-tugas yang berbeda ke dalam fungsi-fungsi terpisah.
![Jawaban-nomer-tiga](https://gitlab.com/Mohasbirizqulloh/tugascloneaplikasiweb/-/raw/main/SS/SRP.gif)

- Open/Closed Principle (OCP): Kode ini menggunakan createSlice dari Redux Toolkit yang membantu mengimplementasikan prinsip OCP. Dalam bagian extraReducers, fungsi-fungsi reducer ditambahkan menggunakan builder.addCase, yang memungkinkan penambahan reducer tambahan tanpa merubah struktur utama NetflixSlice. Ini memungkinkan fleksibilitas dalam memperluas fungsionalitas tanpa harus memodifikasi kode yang sudah ada.
![Jawaban-nomer-tiga](https://gitlab.com/Mohasbirizqulloh/tugascloneaplikasiweb/-/raw/main/SS/OCP.gif)

- Liskov Substitution Principle (LSP): Prinsip LSP menyatakan bahwa objek dari kelas turunan harus dapat digunakan sebagai pengganti objek kelas induk tanpa mengganggu kebenaran program. Dalam kasus ini, Kode tersebut mendefinisikan komponen React bernama BackgroundImage yang menggunakan gambar latar belakang untuk mengisi seluruh area layar.
![Jawaban-nomer-tiga](https://gitlab.com/Mohasbirizqulloh/tugascloneaplikasiweb/-/raw/main/SS/LSP.gif)

- Interface Segregation Principle (ISP): Interface SelectGenreProps hanya memiliki properti yang diperlukan untuk komponen SelectGenre, yaitu genres dan type. Properti tersebut digunakan untuk menampilkan pilihan genre pada elemen <select>. Interface FetchDataByGenrePayload juga hanya memiliki properti yang relevan untuk melakukan fetch data berdasarkan genre, yaitu genre (dalam bentuk angka) dan type.
![Jawaban-nomer-tiga](https://gitlab.com/Mohasbirizqulloh/tugascloneaplikasiweb/-/raw/main/SS/ISP.gif)

- Dependency Inversion Principle (DIP): Kode ini menggunakan axios untuk berinteraksi dengan API dan mengambil data. Namun, dependensi ini dikelola di luar komponen ini dan diinjeksi melalui pembuatan instance axios. Ini mematuhi prinsip DIP dengan bergantung pada abstraksi (instance axios) daripada detail implementasi spesifik. menggunakan Redux Toolkit untuk memisahkan tugas-tugas seperti action creation, reducer, dan store configuration. Hal ini juga mendukung prinsip-prinsip SOLID, termasuk DIP, karena Redux Toolkit memfasilitasi penggunaan pendekatan yang mematuhi prinsip-prinsip tersebut.
![Jawaban-nomer-tiga](https://gitlab.com/Mohasbirizqulloh/tugascloneaplikasiweb/-/raw/main/SS/DSP.gif)

# No 4
#### Design Pattern
Singleton: menggunakan metode getInstance() untuk mendapatkan instance tunggal dari kelas App. Ini memastikan hanya ada satu instance App yang berjalan dalam aplikasi. 
- Constructor private: Konstruktor kelas App dinyatakan sebagai private, yang berarti tidak dapat diakses dari luar kelas. Ini mencegah pembuatan instance App dari luar kelas.

- Static Instance: mendefinisikan sebuah static property bernama instance di kelas App. Properti ini digunakan untuk menyimpan instance tunggal dari kelas App yang akan dibuat.

- Static getInstance() method: Metode getInstance() adalah metode statis yang mengembalikan instance tunggal dari kelas App. Jika instance belum ada, maka metode ini akan membuat instance baru menggunakan konstruktor private. Jika instance sudah ada, maka metode ini akan mengembalikan instance yang sudah ada.
![Jawaban-nomer-empat](https://gitlab.com/Mohasbirizqulloh/tugascloneaplikasiweb/-/raw/main/SS/Singletone.gif)

# No 5
#### Konektivitas ke database
konektivitas ke database MongoDB diimplementasikan menggunakan Mongoose, sebuah library ODM (Object Data Modeling) yang mempermudah interaksi dengan database MongoDB menggunakan JavaScript.

Berikut adalah penjelasan lebih rinci tentang bagaimana konektivitas ke database dilakukan dalam kode saya:

- Import Mongoose: Pertama, mengimpor library Mongoose dengan menggunakan pernyataan const mongoose = require('mongoose');.

- Membuat Koneksi: Selanjutnya, menggunakan metode mongoose.connect() untuk membuat koneksi ke database MongoDB. Metode ini menerima URI koneksi sebagai argumen pertama dan opsi koneksi sebagai argumen kedua. Dalam contoh kode, URI koneksi adalah 'mongodb://localhost:27017/netflix', yang menunjukkan bahwa aplikasi akan terhubung ke server MongoDB yang berjalan secara lokal di port 27017 dengan nama database "netflix". Opsi koneksi yang digunakan dalam contoh ini adalah { useNewUrlParser: true, useUnifiedTopology: true } yang memberitahu Mongoose untuk menggunakan parser URL baru dan menggunakan topologi server MongoDB yang terpadu.

- Menangani Keberhasilan dan Kesalahan: Setelah metode mongoose.connect() dipanggil, Selanjutnya menggunakan metode .then() untuk menangani keberhasilan koneksi. Jika koneksi berhasil, Akan mencetak pesan "DB Connection Successful" menggunakan console.log(). Jika terjadi kesalahan dalam koneksi, akan menangkap pesan kesalahan menggunakan .catch() dan mencetak pesan kesalahan tersebut menggunakan console.log().
![Jawaban-nomer-lima](https://gitlab.com/Mohasbirizqulloh/tugascloneaplikasiweb/-/raw/main/SS/Database.gif)

![Jawaban-nomer-lima](https://gitlab.com/Mohasbirizqulloh/tugascloneaplikasiweb/-/raw/main/SS/DB.gif)

# No 6
#### Web Service
Kode ini merupakan konfigurasi dari sebuah web service menggunakan Express.js. Express.js adalah kerangka kerja Node.js yang digunakan untuk membangun aplikasi web, termasuk web service. Kode ini mendefinisikan beberapa endpoint HTTP menggunakan router Express, yang akan mengarahkan permintaan dari klien ke metode yang sesuai di dalam UserController. Dalam hal ini, kode memiliki tiga endpoint:
 - GET /liked/:email akan memicu metode getLikedMovies dalam UserController untuk mengambil daftar film yang disukai oleh pengguna dengan email tertentu.
 - POST /add akan memicu metode addToLikedMovies dalam UserController untuk menambahkan film ke daftar film yang disukai oleh pengguna.
 - PUT /remove akan memicu metode removeFromLikedMovies dalam UserController untuk menghapus film dari daftar film yang disukai oleh pengguna.
![Jawaban-nomer-enam](https://gitlab.com/Mohasbirizqulloh/tugascloneaplikasiweb/-/raw/main/SS/Web%20Service%20.gif)

#### Setiap operasi CRUD nya
Kelas IUserController adalah interface yang mendefinisikan tiga metode untuk melakukan operasi CRUD (Create, Read, Update, Delete) terkait daftar film yang disukai oleh pengguna berdasarkan alamat email.
 - Create (addToLikedMovies): Metode addToLikedMovies digunakan untuk menambahkan film ke daftar film yang disukai oleh pengguna berdasarkan alamat email. 
 - Read (getLikedMovies): Metode getLikedMovies digunakan untuk mendapatkan daftar film yang disukai oleh pengguna berdasarkan alamat email. 
 - Delete (removeFromLikedMovies): Metode removeFromLikedMovies digunakan untuk menghapus film dari daftar film yang disukai oleh pengguna berdasarkan alamat email dan ID film.
![Jawaban-nomer-enam](https://gitlab.com/Mohasbirizqulloh/tugascloneaplikasiweb/-/raw/main/SS/CRUD.gif)

# No 7
#### Graphical User Interface (GUI)
Untuk GUInya saya menggunakan react typescript, berikut ini tampilan struktur kodingan serta tampilan dari website yang sudah saya buat
![Jawaban-nomer-tujuh](https://gitlab.com/Mohasbirizqulloh/tugascloneaplikasiweb/-/raw/main/SS/STRUKTUR.gif)

![Jawaban-nomer-tujuh](https://gitlab.com/Mohasbirizqulloh/tugascloneaplikasiweb/-/raw/main/SS/GUI.gif)

# No 8
#### HTTP connection melalui GUI produk digital 
kode ini, menggunakan library axios untuk melakukan HTTP connection. axios adalah library JavaScript yang digunakan untuk membuat request HTTP dari sisi klien (client-side). Berikut adalah penjelasan singkat tentang HTTP connection pada kode tersebut:

- Terdapat import axios yang digunakan untuk mengimpor library axios yang sudah diinstal sebelumnya.
- Terdapat definisi interface Movie dan Genre yang digunakan untuk menentukan tipe data dari response yang diterima dari API.
- Terdapat definisi interface NetflixState yang merupakan state dari aplikasi Redux.
- Terdapat definisi initial state initialState yang akan digunakan pada reducer.
- Terdapat definisi async thunks getGenres, fetchDataByGenre, fetchMovies, getUsersLikedMovies, dan removeMovieFromLiked yang digunakan untuk melakukan asynchronous operations seperti fetching data dari API.
- Terdapat definisi NetflixSlice yang merupakan slice dari Redux untuk mengatur state dan reducers terkait dengan fitur Netflix.
- Terdapat definisi store dengan menggunakan configureStore dari Redux Toolkit dan menggabungkan NetflixSlice.reducer sebagai reducer utama.
- Terdapat export setGenres dan setMovies sebagai actions yang dapat digunakan untuk mengubah state di Redux store.

Secara keseluruhan, kode ini menggunakan axios untuk melakukan koneksi HTTP dengan menggunakan metode GET, POST, PUT, dan DELETE ke berbagai endpoint API seperti untuk mendapatkan genre film, data film berdasarkan genre, data film yang sedang trending, daftar film yang disukai oleh pengguna, dan lainnya. Hasil dari request tersebut akan mengubah state pada Redux store menggunakan reducers yang didefinisikan dalam NetflixSlice.
![Jawaban-nomer-delapan](https://gitlab.com/Mohasbirizqulloh/tugascloneaplikasiweb/-/raw/main/SS/HTTP.gif)

# No 9
Link youtube demonstrasi gambaran umum aplikasi saya https://www.youtube.com/watch?v=2sOgFagX_zY
# No 10
#### Bonus Machine Learning
Perlu belajar lebih banyak lagi untuk mengimplementasikannya :) :) 
